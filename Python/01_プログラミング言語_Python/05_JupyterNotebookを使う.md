05 JubyterNotebookを使う
=======================

## JubyterNotebook

* Webブラウザを使ってPythonのコードを入力して実行することができる



### JubyterNotebookを使用する

1. `カーネル`(Pythonのプログラム)を起動する

  * Webブラウザとカーネルが通信をすることで動く

1. `Anaconda Navigator`を起動する

1. `JubyterNotebook`の`Launch`ボタンをクリックする

  * この時、環境設定は`tf140`にする

  -> Python3で設定されているため

1. ダッシュボード上で、`Notebook`を使ってWebブラウザの「New」メニューから「Python3」を選ぶ

  * この時、拡張子は`.ipynb`となる

1. `In[]`と書かれているセルに、コードを入力する

1. コードを入力したら、`Shift`キー＋`Enter`で実行する

* markdownに設定すると、Latexが適用できる



### JubyterNotebookを保存する

1. `File`をクリックする

1. `Save and checkpoint`をクリックする



### Notebookを終了する

1. `File`をクリックする

1. `Close and Halt`(閉じて終了)を選ぶ



| 版 | 年/月/日 |
|---|----------|
|初版|2018/12/15|
